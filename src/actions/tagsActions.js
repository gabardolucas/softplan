import { TAGS_FETCH_ACTION, TAGS_FETCH_ACTION_SUCCESS, TAGS_FETCH_ACTION_ERROR, TAGS_ADD_ACTION } from './actionTypes';

export const tagsFetchAction = () => ({
  	type: TAGS_FETCH_ACTION
});

export const tagsFetchActionSuccess = tags => ({
  	type: TAGS_FETCH_ACTION_SUCCESS,
  	tags
});

export const tagsFetchActionError = () => ({
  	type: TAGS_FETCH_ACTION_ERROR
});

export const tagsAddAction = tag => ({
  	type: TAGS_ADD_ACTION,
  	tag
});