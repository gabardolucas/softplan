import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { configureStore } from './store';
import Container from "./components/Container/Container";

class App extends Component {
	render () {
		return(
			<Container />
		)
	}
}

const rootElement = document.getElementById("root");
ReactDOM.render(<Provider store={configureStore()}><App /></Provider>, rootElement);
