import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tagsFetchAction, tagsFetchActionSuccess, tagsAddAction } from "../../actions/tagsActions";
import { cardsSelectTagAction } from "../../actions/cardsActions";

export class Tags extends Component {
    constructor(props) {
        super(props)
        this.state = { creating: false };
        this.inputTag = React.createRef()
    }

    componentDidMount() {
        const { tagsFetchAction } = this.props;
        tagsFetchAction();
    }

    handleAdicionar = () => {
        this.setState({ creating: true });
    }

    handleCriar = () => {
        const { tagsAddAction } = this.props;
        const id = new Date();
        tagsAddAction({
            "id": id.getTime(),
            "name": this.inputTag.current.value,
            "color": "#fff",
            "background": "#c483d7"
        })
        this.setState({ creating: false })
    }

    handleSeleciona = (tagId) => {
        const { cardsSelectTagAction } = this.props;
        cardsSelectTagAction(tagId);
    }

    cardPerTag = (tagId) => {
        const { cards} = this.props;
        const filtered = cards.filter(({ tag: tagCard }) => tagCard.filter(tag => tag == tagId).length);
        return filtered.length;
    }

    render() {
        const { tags, loadingTags, loadingCards } = this.props;
        const { creating } = this.state;

        if (loadingTags || loadingCards) {
            return (
                <div>Carregando...</div>
            )
        }

        return (
            <div>
                <button onClick={() => { this.handleSeleciona(null) }}>Todos os processos</button>
                <h3>Etiquetas</h3>
                <ul>
                    {tags.map(tag => <li key={tag.id}><button onClick={() => { this.handleSeleciona(tag.id) }}>{tag.name} {this.cardPerTag(tag.id)}</button></li>)}
                </ul>
                {!creating && <div><button onClick={this.handleAdicionar}>Adicionar etiqueta</button></div> }
                {creating && <div><input type="text" ref={this.inputTag}/> <button onClick={this.handleCriar}>Criar</button></div> }
            </div>
        )
    }
}

const mapStateToProps = store => ({
    tags: store.tagsReducer.tags,
    loadingTags: store.tagsReducer.loading,
    cards: store.cardsReducer.cards,
    loadingCards: store.cardsReducer.loading,
});

const mapDispatchToProps = { tagsFetchAction, tagsFetchActionSuccess, tagsAddAction, cardsSelectTagAction };

export default connect(mapStateToProps, mapDispatchToProps)(Tags);

