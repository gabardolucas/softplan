import React, { Component } from 'react';
import Cards from "../Cards/Cards";
import Tags from "../Tags/Tags";
import "./style.scss";


export class Container extends Component {
	render () {
		return(
			<div>
				<div className="tags">
					<Tags />
				</div>
				<div className="container">
					<Cards />
				</div>
			</div>
		)
	}
}

export default Container;