import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { fetchCardsAction, fetchCardsActionSuccess, cardsAddTagAction, cardsRemoveTagAction } from './../../actions/cardsActions';
import "./style.scss";

export class Cards extends Component {
    constructor(props) {
        super(props);
        this.inputTag = React.createRef();
    }

    componentDidMount() {
        const { fetchCardsAction } = this.props;
        fetchCardsAction();
    }

    handleAddTag = (tagId, cardId) => {
        const { cardsAddTagAction } = this.props;
        cardsAddTagAction(tagId, cardId);
    }

    handleRemoveTag = (tagId, cardId) => {
        const { cardsRemoveTagAction } = this.props;
        cardsRemoveTagAction(tagId, cardId);
    }

    filterCards = () => {
        const { cards, selectedTag } = this.props;
        const filtered = cards.filter(({ tag: tagCard }) => tagCard.filter(tag => tag == selectedTag).length);
        return filtered;
    }

    existInCard(tags, tag) {        
        return tags.filter(elm => elm == tag).length;
    }

    render() {
        const { cards, tags, selectedTag, loadingTags, loadingCards } = this.props;
        const haveFilter = !!selectedTag;
        const arrCards = haveFilter ? this.filterCards() : cards;

        if (loadingTags || loadingCards) {
            return (
                <div>Carregando...</div>
            )
        }

        return (
            <div className="cards">
                <div className="list">
                    {arrCards.map((card) => {
                        const { id, partes, classe, assunto, numero, tag: tagCard } = card;

                        return (
                            <div key={id} className="card">
                                <div>{partes.ativa.name} - {partes.passiva.name}</div>
                                <div>{classe} - {assunto}</div>
                                <div>{numero}</div>
                                <div>
                                    {tags.map((tag) => {
                                        const exist = this.existInCard(tagCard, tag.id)
                                        return (
                                            <button className={classNames({ hiddenTag: exist })}  disabled={exist} onClick={() => { this.handleAddTag(tag.id, id) }} key={tag.id} value={tag.id}>{tag.name}</button>
                                        )
                                    })}
                                </div>
                                <div>
                                    {tagCard.map((tag) => {
                                        const currentName = tags.find(currentTag => currentTag.id == tag)
                                        return (
                                            <span key={tag}>{currentName.name} <button onClick={() => this.handleRemoveTag(tag, id)}>(x)</button></span>
                                        )
                                    }
                                    )}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = store => ({
    tags: store.tagsReducer.tags,
    loadingTags: store.tagsReducer.loading,
    cards: store.cardsReducer.cards,
    loadingCards: store.cardsReducer.loading,
    selectedTag: store.cardsReducer.selectedTag,
});

const mapDispatchToProps = {fetchCardsAction, fetchCardsActionSuccess, cardsAddTagAction, cardsRemoveTagAction};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);