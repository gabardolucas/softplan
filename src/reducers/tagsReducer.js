import { TAGS_FETCH_ACTION, TAGS_FETCH_ACTION_SUCCESS, TAGS_FETCH_ACTION_ERROR, TAGS_ADD_ACTION } from '../actions/actionTypes';

const initialState = {
    tags: [],
    loading: false,
    error: false,
};

export const tagsReducer = (state = initialState, action) => {
    switch (action.type) {
        case TAGS_FETCH_ACTION:
            return {
                ...state,
                loading: true,
                error: false,
            };
        case TAGS_FETCH_ACTION_SUCCESS:
            return {
                ...state,
                tags: action.tags,
                loading: false,
                error: false,
            };
        case TAGS_FETCH_ACTION_ERROR:
            return {
                ...state,
                loading: false,
                error: true,
            };
        case TAGS_ADD_ACTION:
            return {
                ...state,
                tags: [ ...state.tags, action.tag ],
            };
        default:
            return state;
    }
};

export default tagsReducer;
