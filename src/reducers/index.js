import { combineReducers } from 'redux';
import { tagsReducer } from './tagsReducer';
import { cardsReducer } from "./cardsReducer";

export const Reducers = combineReducers({
  	tagsReducer: tagsReducer,
  	cardsReducer: cardsReducer,
});